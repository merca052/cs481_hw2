//Name: Jesus Mercado
//Course: CS 481 (Mobile Programming)
//Semester: Fall 2020
//Assignment: Homework 2  (Layout)
import 'package:flutter/material.dart';

//runs the app
void main() => runApp(MyApp());

//Here we declare the main widget for the app
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //Lets adjust the theme of the app
      theme: ThemeData(
        brightness: Brightness.dark,  //gives us dark background and white text
        primaryColor: Colors.green,   //Makes the appBar green
      ),
      //Next let us declare the home screen of the app
      home: Scaffold(
          //We will have an appBar on our home screen
          appBar: AppBar(
              //this appBar will have a title which is centered and says My Pets
              title: Center(
                  child: Text('My Pets'),
                ),
              ),
        //Now let us define the body of the home screen (everything under appBar
        //We declare a Single child scroll view since all our widgets will be
        //declared within a Container
        body:  SingleChildScrollView(
            //We declare a child for this scroll view which will be the
            //container that holds all our widgets
            child: Container(
                //Now in order to add a background image to the home screen, we
                //must make the desired background image, a decoration image for
                //the container.
                //We declare a decoration for the box
                decoration: BoxDecoration(
                    //We declare an image for this decoration
                    image: DecorationImage(
                        //Now we use an asset image as the decoration
                        //image for our main container and we ensure it covers
                        //the entire container.
                        image: AssetImage('assets/Jungle.jpg'),
                        fit: BoxFit.cover,
                        ),
                      ),
                //Within our main container we will have a row as the child
                //(only so we are able to center our column in the app home
                // screen, using just Column as the child for the main container
                //would result in crossAxis Alignment failing to center the
                //column in the app home screen)
                child: Row(
                    //We ensure the main column (column with all information
                    //widgets on the home screen) is centered on phone screen
                    mainAxisAlignment: MainAxisAlignment.center,
                    //we will then declare a child for this row which will be
                    //the main column
                    children: [
                      //Main Info Column for app
                       Column(
                            //We then declare children for the main column which
                            //will be the all the info widgets of the app
                            children: [
                              //Note: We have defined _buildMainContainer(),
                              //_buildInfoContainer(), and
                              //_buildBottomContainer() below under the app
                              //declaration.
                                //Now we add the Main container to our app at
                                //the top of the body, directly under appBar.
                                //This will have the image of all the birds
                                //together, a title, and a brief description
                                // of parakeets
                                _buildMainContainer(),
                                //Under the main container, we will have a text
                                //which will indicate to continue scrolling to
                               //see the info and pic for each individual bird.
                                Text('Lets Meet Them...',
                                    //We want to change the style of this text
                                    style: TextStyle(
                                        //se we declare a font size of 20
                                        fontSize: 20,
                                        //we change it for the theme's default
                                        //white to black.
                                        color: Colors.black,
                                        //We change the font to Times New Roman
                                        fontFamily: 'Times New Roman',
                                    ),
                                  ),
                                //Now, under this Text we will have the
                                //container which will hold all the information
                                //containers for each individual bird (holds
                                //4 containers, 1 for each bird).
                                _buildInfoContainer(),
                                //Under the Info Container we will have a
                                //Container which will act as the artist
                                //signature for the app, it will have CSUSM,
                                //the current semester, the class, and my name.
                                _buildBottomContainer(),
                            ],
                       ),
                    ],
                ),
            ),
        ),
      ),
    );
  }
}
//We have now closed the declaration of the app


//However, lets look into the function definitions we used above. Down below
//will be the definition for each function.
//Lets begin with declaring _buildBirdContainer, which will be the function that
//generates a block container which will hold an image in one column, and the
//personal info of each bird in an adjacent column.
//(Used in _buildInfoContainer() to facilitate the building of each individual
//bird's container.
Widget _buildBirdContainer(String Name, String Picture, String Sex, String Owner, String desc, String v, String f) => Container(
    //NOTE: WE HAVE 7 STRING PARAMETERS (NAME, PICTURE, SEX, OWNER, DESC, V, F),
    //we must use in the function call for this function.
        //Each bird's container will be of size 200 x 350 px.
        height: 200,
        width: 350,
        //The color of the container will be blueGrey, with
        //opacity so we can see through it and see part of the background
        //from the main container of the app
        color: Colors.blueGrey.withOpacity(0.4),
        //Now, for this container we want it to act as a big 200 x 350 px row
        child: Row(
          //In this row we want 2 containers, one with an image which will
          //take up 150 x 200 px of the bird's container starting from the left
          //side. The other will be a container which will act as a column of
          //containers (these containers hold the individual info for each bird)
          //and its view/feed icon and integer
          children: [
              //The 1st container hold the image of the individual bird and
              //ensures that this images takes up 150 x 200 px of the left side
              //of the Bird's Info container (by making the container, the image
              //is in, a certain size) and we want our image to fill this
              //Container.
              Container(
                child:  Image.asset(
                  Picture,
                  width: 150,
                  height: 200,
                  fit: BoxFit.fill,
                  ),
                ),
              //The 2nd container will be adjacent to the 1st container in the
              //Bird's Info container. We will have a column of Row containers
              //which will store two separate texts in a single row with desired
              // spacing
              //NOTE: The semantics/logic for all row containers will be
              //the same so I will only go into detail on the 1st row container
              //and just simply know the rest do the exact same thing but with
              //different strings (i.e Name:, Owner, Sex, etc).
              Container(
                //Referencing the 2nd container in the Bird's Info container,
                //adjacent to the 1st container. We want this container to then
                //fill the rest of the Bird's Info container, so we declare that
                //we want this container to take up 200 x 200 px which takes up
                //the remaining unused width of the Bird's Info Container and it
                //fills the height as well.
                height: 200,
                width: 200,
                    //To have this container act as a column container as
                    //desired, we have the child of the container be a column
                    child: Column(
                        //Within this column container we will then create a
                        //series of row containers which will each hold two
                        //separate column containers in it. Essentially, creating
                        //a row with two columns in it
                        children: [
                        //Lets declare 1st row container for name of the bird
                        Container(
                            //We want the height to adjust based on the text,
                            //so we will not preset the height, but we do want
                            //our width to take up the entire width of our
                            //parent/column container
                            width: 200,
                            child: Row(
                                //Now that we have our row container we want to
                                //create the 2 columns/column containers within
                                //this row. One for the string "Name:" and the
                                //other for the variable Name which is passed
                                //into this function as a string and holds the
                                //string that is the actual name of the bird.
                                children: [
                                    //We do not want to split the row in half,
                                    //instead we want to have the strings
                                    //identifying the info take up only a big
                                    //enough portion of the row to fit the
                                    //biggest ID string (i.e "Fun Fact:")
                                    Container(
                                        //In this case "Fun Fact:" takes aprox.
                                        //60 px, again we do not define height
                                        //because we want it to adjust to text
                                        //height, but we only want the width to
                                        //take up 65 px of the 200 px of the
                                        //row
                                        width: 65,
                                        child: Text('Name:',
                                          //In order to ensure that the text is
                                          //aligned with the right side of this
                                          //container, we align the text to the
                                          //end aka right side of container
                                          textAlign: TextAlign.end,
                                        ),
                                       ),
                                    //Now our second column container within the
                                    //row will take up the remaining 135 px in
                                    //the row and will hold the contain the
                                    //string passed into the function for name
                                    Container(
                                        width: 135,
                                        child: Text(Name,
                                            //To ensure that the text in this
                                            //column of the row is centered
                                            //and not directly next to the text
                                            //in the above container, we align
                                            //the text in this container to the
                                            //center of the container
                                            textAlign: TextAlign.center,
                                           ),
                                        ),
                                   ],
                              ),
                           ),
                          //Lets declare 2nd row container for sex of the bird
                          Container(
                              width: 200,
                              child: Row(
                              children: [
                                    Container(
                                        width: 65,
                                        child: Text('Sex:',
                                            textAlign: TextAlign.end,
                                        ),
                                    ),
                                    Container(
                                        width: 135,
                                        child: Text(Sex,
                                            textAlign: TextAlign.center,
                                        ),
                                    ),
                                ],
                              ),
                          ),
                          //Lets declare 3rd row container for Owner of the bird
                          Container(
                              width: 200,
                              child: Row(
                                  children: [
                                      Container(
                                          width: 65,
                                          child: Text('Owner:',
                                              textAlign: TextAlign.end,
                                          ),
                                      ),
                                      Container(
                                          width: 135,
                                          child: Text(Owner,
                                              textAlign: TextAlign.center,
                                                ),
                                      ),
                                  ],
                              ),
                          ),
                          //Lets declare 4th row container for Fun Fact of bird
                          Container(
                              width: 200,
                              child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                      Container(
                                          width: 65,
                                          child: Text('Fun Fact:',
                                              textAlign: TextAlign.end,
                                            ),
                                      ),
                                      Container(
                                          //Helps ensure that the smaller text
                                          //size difference does not affect the
                                          //formatting of the 2nd column
                                          //container (smaller text starts at
                                          //the same height as the larger text
                                          //in the 1st column container for this
                                          //row
                                          padding: const EdgeInsets.only(
                                                top: 3.5,
                                                left: 2,
                                                right: 2),
                                        width: 135,
                                        //Note: Here we do limit the height
                                        //because we want the bottom 80 px of
                                        //the Main column, that holds all of
                                        //these row columns, to be left
                                        //available for our view/feed
                                        //Icons and data
                                        height: 80,
                                        child: Text(desc,
                                            //We want our text to form a block
                                            //within the given column container
                                            //space. So, we justify the
                                            //alignment to get the text to fill
                                            //the box evenly and since we deal
                                            //with a block of text we lower the
                                            //font size
                                            textAlign: TextAlign.justify,
                                            style: TextStyle(
                                                  fontSize: 10
                                            ),
                                        ),
                                      ),
                                    ],
                              ),
                          ),
                          //Now under the 4 row containers for the bird's info,
                          //we will have a container which takes up the bottom
                          //200 x 72 px of the parent column container, which
                          //holds all these row containers. Inside of this
                          //container we will have two Icons with data
                          //underneath(in a column).
                          Container(
                              width: 200,
                              height: 72,
                              //These icons will be in a
                              //row and spaced evenly in this container at the
                              //bottom of the column container/Bird's
                              //Info container.
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  //Below we have _buildViewContainer(str v) and
                                  //_buildFeedContainer(str f) defined below.
                                  //They each take a parameter from those passed
                                  //into this function (the last 2). and thus
                                  //create each icon with the correct numerical
                                  //data for each bird.
                                  children: [
                                      _buildViewContainer(v),
                                      _buildFeedContainer(f),
                                  ],
                              ),
                          ),
                      ],
                    ),
              ),
          ],
        ),
);
//Now we have declared the function for building each individual bird's
//info container


//Lets now define the _buildMainContainer() which takes no parameters since
//everything in this container is explicitly defined.
Widget _buildMainContainer() => Container(
      //We will make this a column container, so that it can hold the Main block
      //of the app in a column since we want our widgets underneath one another
      child: Column(
          //To add each widget we will put them in containers within this column
          children: [
              //The 1st container will hold the image of all 4 birds together
              Container(
                  //To ensure that the image will always take up the entire
                  //width of any screen and only a certain height of the screen
                  //we must envoke a layout builder which will return a
                  //container with the image.
                  child: LayoutBuilder(
                        builder: (BuildContext context, BoxConstraints constraints) {
                        return Container(
                            //Here we ensure the above mentioned constraints
                            height: MediaQuery.of(context).size.height / 2.5,
                            width: MediaQuery.of(context).size.width / 1,
                            //Here we add the image to the container and ensure
                            //it covers the entire container/desired area
                            child: Image.asset('assets/Group.jpg',
                                fit: BoxFit.cover,
                            ),
                          //Ensure to close the layout builder with a ";" after
                          //the parenthesis ending the definition of the
                          //returned container
                        );
                    }
                  ),
              ),
             //The 2nd container will hold the title for this main block of info
            Container(
              //ensure that the title is not directly beneath the image with
              //no spacing, we add padding to the top
              padding: const EdgeInsets.only(top: 5),
              child: Text('THE GREAT PARAKEETS OF MY HOME',
                  //We want this text to be of Times New Roman font, size 20,
                  //bold, and of color blueGrey.
                  style: TextStyle(
                      fontFamily: 'Times New Roman',
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.blueGrey,
                    ),
                ),
            ),
            //The 3rd container will hold the paragraph with info for this Main
            //Container
            Container(
                //To ensure the block of text does not run from end to end
                //on the screen and to ensure that it is not directly under the
                //title with no space, we once again add padding
                padding: const EdgeInsets.only(
                      left: 10,
                      right: 10,
                      top:10
                  ),
                //We want this text box to only take up 200 x 380 px under
                //the title and image.
                height: 200,
                width: 380,
                child: Text(
                  'Parakeets are part of many small to medium-sized species of parrots, '
                      'generally they have a long tail and feathers. '
                      'Parakeet is derived from the French word perroquet. However, '
                      'in French parakeet is perruche. Parakeets often breed '
                      'in groups, because they are encouraged by the presence of other parakeets. '
                      'There could be conflicts between breeding pairs and even individuals if '
                      'space is limited. The presence of other parakeets '
                      'encourages a pair to breed, which is why breeding in groups is better.',
                    //Now to ensure our app doesn't do anything funky with this
                    //paragraph, we want to let the system know that if there is
                    //too many words for the available space, then use elipses
                    //after the last word that could fit in the container. Also,
                    //justify the text so it will come out in a block filling
                    //up the container. We also state that we want a max of 14
                    //lines, so if the text takes 15 lines the last line will be
                    //taken out and replaced with elipses
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.justify,
                    maxLines: 14,
                    //We want the font to be of size 14 and color black45
                    //(lighter shade of black)
                    style: TextStyle(fontSize: 14, color: Colors.black45),
                    //To ensure the text looks the same for different screen
                    //sizes, we use a soft wrap.
                    softWrap: true,
                ),
            ),
          ],
      ),
);
//Now, we have finished declaring the _buildMainContainer(), which builds the
//section of info which pertains to all birds together with general info on them
//as a species.


//Here we declare predefined strings which we can pass into function calls for
//building the bird's individual info containers
//(_buildBirdContainer(..., String desc,...)).
//This way we can avoid writing this long string in the function call and we can
//use the naming of these strings to know which bird's container we are creating
var Pharaohdesc = 'He is the smartest of the group, when the cage opens for them to go out you can always find Pharaoh leaving and entering on his own. No assistance needed. ';
var Blueberrydesc = 'She is the oldest of them. She loves to dig her nest and when she is loose around the house she loves to fly (even more than the rest).';
var Spiderdesc = 'He is the fluffiest if the group. He loves to sleep and eat his favorite food (Millets). When they are out of the cage he prefers to lounge around. Contrary to his name. ';
var Pepitadesc = 'She is the youngest and most agile of the bunch. It is always a task trying to catch Pepita when its time to go back in the cage. She is the first up and showered';


//Now lets build the column container which will hold the 4 individual Bird
//info containers [ _buildInfoContainer() ],  and be placed directly under the
//text "Lets Meet Them..." which is under the build for the MainContainer
Widget _buildInfoContainer() => Container(
    //We want this column containers's width to be the same as the width of each
    //bird's info container, so we do not predefine it for this container.
    //However, we do want our container to have a height greater than the height
    //of all 4 bird info containers stacked on top of each other. So we use 900
    //px in this case
    height: 900,
    child: Column(
        //We want these bird info containers to be spaced evenly within this
        //container
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children:[
          //and we then build each individual bird's info container
            _buildBirdContainer('Pharaoh', 'assets/Pharaoh.jpeg', 'Male', 'Me', Pharaohdesc, '22', '10'),
            _buildBirdContainer('Blueberry', 'assets/Blueberry.jpg', 'Female', 'Sister', Blueberrydesc, '10', '5'),
            _buildBirdContainer('Spider-Man', 'assets/Spider-Man.jpeg', 'Male', 'Brother', Spiderdesc, '18', '12'),
            _buildBirdContainer('Pepita', 'assets/Pepita.jpeg', 'Female', 'Mom', Pepitadesc, '15', '8'),
         ],
    ),
);
//Now we have declared the _buildInfoContainer() which takes no parameters and
//builds/formats the 4 individual bird info containers.


//We then will declare the _buildBottomContainer() which acts as an artist
//signature for the app and lies at the very bottom of
//Main Container(has background image). It has the school name, semester,
// course, and my name
Widget _buildBottomContainer() => Container(
    //We will want this to take up the bottom 60 px of vertical space on the
    //Main Container and we want it to expand almost the entire screen length
    height: 60,
    width: 300,
    //To get the signature in lines underneath one another we make this a
    //column container
    child: Column(
      //We align the signature to the left bottom side of the screen
      crossAxisAlignment: CrossAxisAlignment.start,  //Row start = left side
      children: [
          //We then declare each text segment of the artist signature
          //School name and semester:
          Text('CSUSM: Fall 2020',
              //Now we want each text segment to be white, bold, and
              //50% transparent
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white.withOpacity(0.5),
              ),
          ),
          //Now we want to declare the
          //Course:
          Text('CS 481: Mobile Programming',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white.withOpacity(0.5),
              ),
          ),
        //Now we want to declare my
        //Name:
          Text('Jesus Mercado',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white.withOpacity(0.5),
              ),
          ),
      ],
    ),
);
//Now we have finished declaring the artist signature for yhe app which will be
//placed at the very bottom of the app.


//Now let us declare the _buildViewContainer(view) and _buildFeedContainer(feed)
//They follow the exact same logic, only different icon name and id strings.
Widget _buildViewContainer(String view) => Container(
    //We want our icon and data to be within a column container of size
    //60 x 30 px. So that it fits almost the entire vertical area of the
    //container it will be built inside of.
    height: 60,
    width: 30,
    child: Column(
      //We want this icon container to begin from the top of the parent
      //column container and we want the objects to align to the center of a row
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
          //We declare the Icon at the top of this small column container
          //visibility = eye.
          Icon(Icons.visibility),
          //We declare a string which represents an integer to display the
          //number views this specific bird has gotten. Under the
          //Icon and centered.
          Text(view),
          //We declare a explicit string which states what the icon means and
          //gives a unit to the integer string, in this case the unit is views.
          //This is directly under the printed view parameter value under
          //the icon, and it is also centered with size 10 font.
          Text('views',
                style: TextStyle(
                      fontSize: 10
                ),
          ),
      ],
    ),
);
//Now we have finished the definition of the View Icon which displays how many
//views a particular bird has gotten.


//So, we go on to define _buildFeedContainer(feed) which will build the Icon and
//display the integer string and unit for # of feeds a bird has gotten
Widget _buildFeedContainer(String feed) => Container(
    height: 60,
    width: 30,
    child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
            //fastfood = burger in front of soda cup icon
            Icon(Icons.fastfood),
            Text(feed),
            //units = feeds
            Text('feeds',
                style: TextStyle(
                    fontSize: 10
                ),
            ),
        ],
    ),
);
//We finished declaring the function definition for building the feed icon

//Code is complete